import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Product from "./Components/Product";
import Homepage from "./Components/Homepage";
import Categories from "./Components/Categories";
import Announcement from "./Components/Announcement";
import WomenCategories from "./Components/WomenCategories";
import MenCategories from "./Components/MenCategories";
import JewelleryCategories from "./Components/JewelleryCategories";
import Gadgets from "./Components/Gadgets";
import AboutUs from "./Components/AboutUs";
import OurWork from "./Components/OurWork";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productArr: [],
      isDataFetched: false,
      error: false,
      loadingData: true,
    };
  }

  componentDidMount = () => {

    fetch('https://fakestoreapi.com/products')
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          productArr: data,
          isDataFetched: true,
          loadingData: false,
          error: false

        })

        fetch('https://fakestoreapi.com/products', {
          method: "POST",
          headers: {
            'Accept': 'application/json, text/plain,  ',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(
            {
              title: 'test product',
              price: 13.5,
              description: 'lorem ipsum set',
              image: 'https://i.pravatar.cc',
              category: 'electronic'
            }
          )
        })
          .then(res => res.json())
          .then(newData => {


            this.setState({
              productArr: [...this.state.productArr, newData],
              isDataFetched: true,
              loadingData: false,
              error: false
            })
          })

      }).catch((err) => {
        this.setState({
          error: true,
          loadingData: false
        })
      })
  }

  render() {
    console.log(this.state.productArr)
    return (
      <>
        <Announcement />

        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/products" element={<Product data={this.state} />} />
          <Route path="/collections" element={<Categories />} />
          <Route path="/men-categories" element={<MenCategories data={this.state} />} />
          <Route path="/women-categories" element={<WomenCategories data={this.state} />} />
          <Route
            path="/jewellery-categories"
            element={<JewelleryCategories data={this.state} />}
          />
          <Route path="/electronics-categories" element={<Gadgets data={this.state} />} />
          <Route path="/aboutus" element={<AboutUs />} />
          <Route path="/ourwork" element={<OurWork />} />
        </Routes>
      </>
    );
  }
}

export default App;
