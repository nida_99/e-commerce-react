import React from 'react';
import './style.css'
import Cssloader from "./Cssloader";
import HomeNav from './HomeNav';
import { Link } from 'react-router-dom'
import SingleProduct from './SingleProduct';



class Product extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            array: this.props.data.productArr,
            selectedProduct: null,
            update: false
        }
    }



    deleteProduct = (product) => {


        const newData = this.state.array.filter((ele) => {
            // console.log("ele-id", ele.id)
            if (ele.id !== product.id) {
                return true
            }
        })


        this.setState({
            array: newData
        })

    }



    changeData = (changeData, index) => {


        let res = this.state.array;


        const obj = {
            title: changeData.title,
            price: changeData.price,
            rating: { rate: changeData.rate },
            image: res[index].image,
        };
        console.log(obj)

        res[index] = obj;

        this.setState({
            productData: res,
        })
    }


    render() {


        return (
            <>
                <HomeNav />

                <div className='all-prod'>ALL PRODUCTS</div>
                {(this.props.data.loadingData === true) ? <Cssloader /> :

                    (this.props.data.error === true) ? <h1> Error occured!</h1>

                        :

                        (this.state.array.length === 0) ? <h1>No products found</h1> :

                            <div className='whole-container'>

                                {this.state.array.map((ele, index) => {

                                    return (
                                        <>
                                            <div key={ele.id} className='single-product'>

                                                <SingleProduct src={ele.image} description={ele.title} price={ele.price} id={ele.id} ele={ele} state={this.state}
                                                    rate={ele.rating ? ele.rating.rate : 0} updateItem={this.updateItem} deleteProduct={this.deleteProduct}
                                                    changeData={this.changeData}
                                                    index={index}
                                                />
                                            </div>


                                        </>
                                    )

                                })}

                            </div>

                }


            </>
        )

    }

}
export default Product;
