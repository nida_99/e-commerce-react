import './Cssloader.css'

import React from "react";
import './Cssloader.css'

class Cssloader extends React.Component {

    render() {
        return (
            <div className='loader'>
                <div className="lds-dual-ring"></div>

            </div>
        )
    }
}

export default Cssloader;