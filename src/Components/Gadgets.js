import React from 'react';
import { Link } from 'react-router-dom'
import Cssloader from "./Cssloader";
import HomeNav from './HomeNav';
import ShowProduct from './ShowProduct';

class GadgetsCategories extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <>
                <HomeNav />
                {
                    (this.props.data.loadingData === true) ? <Cssloader /> :
                        (this.props.data.error === true) ? <h1> Error occured!</h1> :
                            <div className='whole-container'>
                                {this.props.data.productArr.map((ele) => {
                                    if (ele.category === "electronics" || ele.category === "electronic") {
                                        return (
                                            <div key={ele.id}>
                                                <ShowProduct src={ele.image} description={ele.title} price={ele.price} id={ele.id} rate={ele.rating ? ele.rating.rate : 0} />
                                            </div>
                                        )
                                    }
                                })
                                }
                                {/* <NewProduct /> */}
                            </div>

                }

            </>)

    }

}
export default GadgetsCategories;
