import React from 'react';
import './style.css'
import { Link } from 'react-router-dom'

class HomeNav extends React.Component {

    render() {
        return (
            <>

                <div className="home-nav">
                    <ul>
                        <li> <Link className="link" to='/'> HOME </Link> </li>
                        <li><Link className="link" to='/products'> PRODUCT</Link></li>
                        <li><Link className="link" to='/collections'> COLLECTIONS</Link></li>
                    </ul>
                </div>

            </>
        )
    }
}


export default HomeNav