import React from 'react';
import './style.css'
class SingleProduct extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: "",
            showForm: false,
            title: this.props.description,
            price: this.props.price,
            rate: this.props.rate
        };
    }

    updData = () => {
        this.setState({
            showForm: !this.state.showForm,
        });
    };

    changeVal = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    changeData = (event) => {
        event.preventDefault();


        this.props.changeData(this.state, this.props.index);

        this.setState({
            showForm: !this.state.showForm,
        });
    };



    deleteData = () => {

        this.props.deleteProduct(this.props);
    }


    render() {

        return (
            <>
                <div key={this.props.id} className='product-details' >
                    <div className='image-product'>
                        <img src={this.props.src} />
                    </div>
                    <div className='product-title'>
                        {this.props.description}
                    </div>

                    <div className='product-price'>
                        $ {this.props.price}
                    </div>

                    <div className='rating'>
                        <div>
                            {this.props.rate} <span>star</span>

                        </div>
                    </div>

                    <div>

                        {this.state.showForm === false ?
                            <div>
                                <div>
                                    <button className='cart-btn'>Add to cart</button>

                                </div>


                                <button className='cart-btn'
                                    onClick={this.updData}>Update product
                                </button>

                                <div>
                                    <button className='cart-btn' onClick={this.deleteData}>Delete product</button>
                                </div>
                            </div>
                            :
                            <div> <div><input name="title" type="text" placeholder='new title' defaultValue={this.props.description} onChange={this.changeVal} /></div>
                                <div>  <input type="number" name="price" placeholder='new price' defaultValue={this.props.price} onChange={this.changeVal} />
                                </div>
                                <div>  <input type="number" name="rate" placeholder='new rating' defaultValue={this.props.rate} onChange={this.changeVal} />
                                </div>
                                <button className='cart-btn' onClick={this.changeData}>Update item</button>
                            </div>
                        }

                    </div>


                </div>


            </>
        )
    }
}
export default SingleProduct;