import React from "react";
import './style.css'
import { Link } from 'react-router-dom'
import Footer from "./Footer";


class Categories extends React.Component {

    render() {

        return (
            <>

                <div className="home-nav-prod">
                    <ul>
                        <li> <Link className="link" to='/'> HOME </Link> </li>
                        <li><Link className="link" to='/products'> PRODUCT</Link></li>
                        <li><Link className="link" to='/collections'> COLLECTIONS</Link></li>
                    </ul>
                </div>
                <div className="categories-component">

                    <div className="categories-men">
                        <div className="extra">
                            <h1> MEN</h1>
                            <Link className="link" to='/men-categories'><button className="category-btn"> SHOP NOW</button></Link>
                        </div>

                    </div>

                    <div className="categories-women">
                        <div className="extra">
                            <h1>WOMEN</h1>
                            <Link className="link" to='/women-categories'><button className="category-btn"> SHOP NOW</button></Link>
                        </div>

                    </div>
                    <div className="categories-jwellery">
                        <div className="extra">
                            <h1>ACCESSORIES</h1>
                            <Link className="link" to="/jewellery-categories"><button className="category-btn"> SHOP NOW</button></Link>
                        </div>

                    </div>
                    <div className="categories-electronics">
                        <div className="extra">
                            <h1> ELECTRONICS</h1>
                            <Link className="link" to="/electronics-categories"><button className="category-btn"> SHOP NOW</button>  </Link>
                        </div>

                    </div>





                </div>
                <Footer />
            </>
        )
    }
}


export default Categories