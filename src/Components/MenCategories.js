import React from 'react';
import Cssloader from "./Cssloader";
import ShowProduct from './ShowProduct';
import { Link } from 'react-router-dom'
import HomeNav from './HomeNav';

class MenCategories extends React.Component {


    constructor(props) {
        super(props)
    }
    render() {

        return (
            <>
                <HomeNav />
                {
                    (this.props.data.loadingData === true) ? <Cssloader /> :
                        (this.props.data.error === true) ? <h1> Error occured!</h1> :
                            <div className='whole-container'>
                                {this.props.data.productArr.map((ele) => {
                                    if (ele.category === "men's clothing") {
                                        return (
                                            <div key={ele.id} >
                                                <ShowProduct src={ele.image} description={ele.title} price={ele.price} id={ele.id} rate={ele.rating.rate} count={ele.rating.count} />
                                            </div>
                                        )
                                    }
                                })
                                }

                            </div>

                }

            </>)

    }

}
export default MenCategories;

