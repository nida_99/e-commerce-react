import React from "react";

class Footer extends React.Component {

    render() {

        return (
            <>

                <div className="footer">

                    <div className="footer-container">
                        <div className="footer-left">
                            <ul>
                                <li>Terms & Conditions</li>
                                <li>Delivery & Returns</li>
                                <li>Sizing Chart</li>
                            </ul>
                        </div>

                        <div className="footer-right">
                            <ul>
                                <li> Contact us</li>
                                <li> +61 (0) 452 604 147 • </li>
                            </ul>
                        </div>

                        <div className="footer-center">
                            <img className="return-image" src='https://constant.myntassets.com/web/assets/img/ef05d6ec-950a-4d01-bbfa-e8e5af80ffe31574602902427-30days.png' /><span>Return within 30days</span>
                            <img className="original" src="https://constant.myntassets.com/web/assets/img/6c3306ca-1efa-4a27-8769-3b69d16948741574602902452-original.png" /><span>100% Original</span>
                        </div>
                    </div>

                    <p className="footer-para">
                        <span>@2022</span>, Barrel | Indian Made Sustainable Clothing
                        Ecommerce Software.
                    </p>

                </div>



            </>
        )
    }

}


export default Footer;