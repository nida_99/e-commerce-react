import React from 'react';
import Announcement from './Announcement';
import Footer from './Footer';
import HomeNav from './HomeNav';
import { Link } from 'react-router-dom'

class AboutUs extends React.Component {
    render() {

        return (
            <>
                <HomeNav />

                <div className='about-container'>
                    <div className='about-title'> ABOUT US</div>
                    <div className='about-image'>
                        <img src="https://cdn.shopify.com/s/files/1/0049/6922/files/Seeds_Banner_grande.jpg?v=1490060603"></img>
                    </div>
                    <div className='about-header'>
                        <div className="about">
                            <ul>
                                <li>
                                    Designed with integrity and made to last, our clothes are made for living in!
                                </li>
                                <li>
                                    We care about the environment, support local industry and work with organic and sustainable fabrics.
                                </li>

                                <li>
                                    Built for layering our pieces are easy wear, easy care, trans-seasonal essentials; luxe staples with a contemporary twist that continue their life from season to season.
                                </li>

                                <li>
                                    To us sustainability is all about making conscious design decisions.
                                </li>

                                <li>
                                    Based in India, Barrel was founded in 2006 by Rebecca Powell whose career spans over 3 decades working with, established Australian and international brands.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <Footer />

            </>
        )
    }
}

export default AboutUs